<!------- Traitement Authentification---->
<?php
$erreurinscription=null;
if(isset($_POST['connectionadmin']))
  {
    $emailconnection=htmlentities(trim($_POST['emailconnection']));
    $passwordconnection=$_POST['passwordconnection'];
    
    if($passwordconnection&&$emailconnection)
    {
        if($passwordconnection=="admin"&&$emailconnection=="admin@admin.com") {
        session_start();
        $_SESSION['emailconnection'] = $_POST['emailconnection'];
            header('Location: page_admin.php');
            exit();
          }else{
            $erreurinscription="L'email ou le mot de passe est incorrect";
       } 
      }else $erreurinscription="Remplissez tous les champs" ;
    }
    
?>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <title> Se connecter </title>
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body id="connectionbody">
  <!-----Barre de Navigation---->
	<section id="Navigation" >
		<nav class="navbar navbar-expand-lg navbar-light ">
  <a class="navbar-brand" href="#"><img src="images/logo3.jpeg"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="connexion.php"> RETOUR </a>
      </li>
      
      
    </ul>
  </div>
</nav>
	</section>
<!-------Authentification---->
<div id="Authentification">
<h2> Se connecter </h2>
<form  method="POST" action="connectionadmin.php" > 
	<div class="form-group row">
    <label for="inputEmail3" class="col-sm-4 col-form-label"> Email </label>
    <div class="col-sm-8">
      <input type="email" class="form-control" name="emailconnection">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword3" class="col-sm-4 col-form-label">Password</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" name="passwordconnection">
    </div>
  </div>
   
  <p> <?php echo $erreurinscription;  ?> </p>
  
  <button type="submit" name="connectionadmin" class="btn btn-primary">Se connecter</button>
</form>
</div>


	<!-- Footer -->
  <footer class="page-footer font-small special-color-dark pt-4">
<!-- Copyright -->
<div class="footer-copyright text-center py-3" color="white" >© 2020 Copyright:
  <a href="home_page.php"> Wearegamer.com</a>
</div>
<!-- Copyright -->
</footer>
</body>
</html>