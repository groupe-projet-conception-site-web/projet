<?php
  // Initialiser la session
  session_start();
  // Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
  if(!isset($_SESSION["emailconnection"])){
    header("Location: home_page.php");
    exit(); 
  }
 
?>
<!----- Traitement ajout image Pour un jeu créé ---->
<?php
if(isset($_POST["env"])){
$logo=$_FILES['photo']['name'];

if($logo!=""){
require "uploadImage.php";
if($sortie==false){$logo=$dest_dossier . $dest_fichier;}
else {$logo="notdid";}
}
if($logo!="notdid"){
}
else{
echo"recommence!!!";
}
}
?>
<!---- Traitement Création d'un nouveau jeu ---->
<?php
if(isset($_POST['creationjeu']))
  {
    $nomjeu=htmlentities(trim($_POST['nomjeu']));
    $description=htmlentities(trim($_POST['description']));
    $choixconsole=htmlentities(trim($_POST['choixconsole']));
    $choixgenre=htmlentities(trim($_POST['choixgenre']));
    $photo=$_FILES['photo']['name'];
    
    if($nomjeu&&$description&&$choixconsole&&$choixgenre)
    {
      if($photo)
      {
    
        $conn = mysqli_connect('localhost','root','root','projet') or die (mysqli_error());

        $req = "INSERT INTO jeu (nom,description,console,genre,image) values ('$nomjeu','$description','$choixconsole','$choixgenre','$photo')";
        
        $res = mysqli_query($conn,$req);

        echo " Création réussi "; 
      } else echo "Vous devez forcément ajouter une image à votre jeu ";


    }else echo "Veuillez saisir tous les champs";

  }

?>



<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <title> Espace Admininistrateur</title>
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
  <!-----Barre de Navigation---->
	<section id="Navigation" >
		<nav class="navbar navbar-expand-lg navbar-light ">
  <a class="navbar-brand" href="home_page.php"><img src="images/logo3.jpeg"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#Navigation"> BIENVENUE CHER Admin </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="deconnection.php"> SE DECONNECTER </a>
      </li>
    </ul>
  </div>
</nav>
  </section>

  <!---- Formulaire Creation jeu ---->
  <section id="CreationJeu">
    <p> Bienvenue dans l'espace Admininistrateur, Ici vous pourrez créer de nouveaux jeux.
      Vous pourrez les afficher et également les supprimer. Afin de voir, les j'aime et les je n'aime pas. 
      Il faudra vous connecter Via un compte utilisateur. 
    </p>

  <h2 id="titrecreerjeu"> Créer un nouveau jeu  </h2>

  <form method="POST" action="page_admin.php" name="formulaire" enctype="multipart/form-data">
<p> Nom du jeu  : </p>
<input type="text" name="nomjeu" class="col-sm-8 col-form-label">
<p> Description  : </p>
<input type="text" name="description" class="col-sm-8 col-form-label">
<p> Choisissez une console : </p>
<select class="custom-select" name="choixconsole">
  <option selected> Console</option>
  <option value="Ps2">Ps2</option>
  <option value="Ps3">Ps3</option>
  <option value="Ps4">Ps4</option>
  <option value="Ps5">Ps5</option>
  <option value="PsVita">PsVita</option>
  <option value="3DS">3DS</option>
  <option value="PC">PC</option>
  <option value="Xbox360">Xbox360</option>
  <option value="XboxOne">XboxOne</option>
</select>

<p> Choisissez le genre  : </p>
<select class="custom-select" name="choixgenre">
  <option selected> Genre</option>
  <option value="action">action</option>
  <option value="sport">sport</option>
  <option value="arcade">arcade</option>
  <option value="RPG">rpg</option>
  <option value="Course">Course</option>
</select>

<p> Ajouter une image : </p>
<input type="file" name="photo" id="photo" /> </div>
<input type="hidden" value="b" name="env"/>
<input type="submit" value="Creer Nouveau Jeu" class="btn btn-primary" name="creationjeu">
</form>

<a href="page_liste_jeux.php"> Afficher les jeux et Supprimer </a>
</section>

<footer class="page-footer font-small special-color-dark pt-4">
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href=""> Wearegamer.com</a>
  </div>

</footer>
</body>
</html>