<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <title> Accueil </title>
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
  <!-----Barre de Navigation---->
	<section id="Navigation" >
		<nav class="navbar navbar-expand-lg navbar-light ">
  <a class="navbar-brand" href="home_page.php"><img src="images/logo3.jpeg"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#Navigation"> ACCUEIL </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#about"> A PROPOS </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#inscription"> INSCRIPTION </a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="connexion.php"> SE CONNECTER</a>
      </li>
    </ul>
  </div>
</nav>
	</section>
	<!--------Slider de Bienvenue ------->
	<div id="Slider">
		<div id="headslider" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#headslider" data-slide-to="0" class="active"></li>
    <li data-target="headslider" data-slide-to="1"></li>
    <li data-target="#headslider" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/image3.jpeg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>  Bienvenue sur Wearegamer.com </h5>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/image2.jpeg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5> Cause we all are game loverr </h5>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/image1.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5> Let's share our love of game !!  </h5>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#headslider" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#headslider" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
<!------ A propos---->
<section id="about">
	<div class="container">
		<div class="row">
			<div class="cpl-md-6">
				<h2> About Us</h2>
				<div id="textabout"> 	Bienvenue sur wearegamer.com ! wearegamer.com : le réseau social des 
          jeux vidéos, le site sur l'actualité du jeu vidéo sur consoles et PC. Retrouvez vos jeux 
          vidéos préférés, likez les et partager vos expériences avec vos amis. </div>
			</div>			
		</div>
	</div>
</section>

<!------- Affichage de quelques jeux--------->
<section id="Affichagejeu">
	<h2> Quelques jeux pour vous  </h2>

	<div id="Slider2">
		<div id="gameslider" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#gameslider" data-slide-to="0" class="active"></li>
    <li data-target="#gameslider" data-slide-to="1"></li>
    <li data-target="#gameslider" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/fortnite.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <p><a href="connexion.php">Connectez vous pour voir plus</a> </p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/fifa21a.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <p> <a href="connexion.php">Connectez vous pour voir plus</a> </p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/gtaonlinea.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <p>  <a href="connexion.php">Connectez vous pour voir plus</a> </p>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#gameslider" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#gameslider" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

</section> 

<!---- inscription---->

<section id="inscription">

<h2> INSCRIPTION </h2> 
<p> Remplissez le formulaire suivant afin de créer un compte </p>
<form method="POST" action="home_page.php">
<label for="nom"> Nom : </label>
<input type="text" name="nom" class="col-sm-4 form-control" id="nom" >
<label for="prenom"> Prenom : </label>
<input type="text" name="prenom" class="col-sm-4 form-control" >
<label for="email"> Email : </label>
<input type="email" name="email"  class="col-sm-4 form-control" aria-describedby="emailHelp">
<label for="password"> Mot de passe : </label>
<input type="password" name="password" class="col-sm-4 form-control" >
<label for="confirmpassword"> Confirmation du mot de passe : </label>
<input type="password" name="confirmpassword" class="col-sm-4 form-control" > <br></br>
<input type="submit" value="S'inscrire" name="submit" class="btn btn-primary">
</form>

</section>

<!---- Traitement Inscription ---->
<?php
if(isset($_POST['submit']))
  {
    $nom=htmlentities(trim($_POST['nom']));
    $prenom=htmlentities(trim($_POST['prenom']));
    $email=htmlentities(trim($_POST['email']));
    $password=sha1($_POST['password']);
    $confirmpassword=sha1($_POST['confirmpassword']);
    
    if($nom&&$prenom&&$email&&$password&&$confirmpassword)
    {
      $conn1 = mysqli_connect('localhost','root','root','projet') or die (mysqli_error());
      // Verifie si l'email est deja utilisé
      $query = "SELECT * FROM membres WHERE email='$email'";
      $result1 = mysqli_query($conn1,$query) or die(mysql_error());
      $rows1 = mysqli_num_rows($result1);
      if($rows1==0)
      {
      if($password==$confirmpassword)
        {
          $conn = mysqli_connect('localhost','root','root','projet') or die (mysqli_error());

           $req = "INSERT INTO membres (nom,prenom,email,password) values ('$nom','$prenom','$email','$password')";
        
          $res = mysqli_query($conn,$req);

          echo " Inscription réussie ";

        } else echo "Les mots de passe doivent etre identiques";
      } else echo "Email déja utilisé";
    }else echo "Veuillez saisir tous les champs";

  }

?>
	<!-- Footer -->
<footer class="page-footer font-small special-color-dark pt-4">
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="#navigation"> Wearegamer.com</a>
  </div>

</footer>
</body>
</html>

