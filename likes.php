<?php
$bdd = new PDO("mysql:host=localhost;dbname=projet;charset=utf8", "root", "root");
if(isset($_GET['t'],$_GET['id']) AND !empty($_GET['t']) AND !empty($_GET['id'])) {
   $getid = (int) $_GET['id'];
   $gett = (int) $_GET['t'];
   $idmembre = (int) $_GET['idmembre'];
   $check = $bdd->prepare('SELECT id FROM jeu WHERE id = ?');
   $check->execute(array($getid));
   if($check->rowCount() == 1) {
      if($gett == 1) {
         $check_like = $bdd->prepare('SELECT id FROM likes WHERE id_jeu = ? AND id_membre = ?');
         $check_like->execute(array($getid,$idmembre));
         $del = $bdd->prepare('DELETE FROM dislikes WHERE id_jeu = ? AND id_membre = ?');
         $del->execute(array($getid,$idmembre));
         if($check_like->rowCount() == 1) {
            $del = $bdd->prepare('DELETE FROM likes WHERE id_jeu = ? AND id_membre = ?');
            $del->execute(array($getid,$idmembre));
         } else {
            $ins = $bdd->prepare('INSERT INTO likes (id_jeu, id_membre) VALUES (?, ?)');
            $ins->execute(array($getid, $idmembre));
         }
         
      } elseif($gett == 2) {
         $check_like = $bdd->prepare('SELECT id FROM dislikes WHERE id_jeu = ? AND id_membre = ?');
         $check_like->execute(array($getid,$idmembre));
         $del = $bdd->prepare('DELETE FROM likes WHERE id_jeu = ? AND id_membre = ?');
         $del->execute(array($getid,$idmembre));
         if($check_like->rowCount() == 1) {
            $del = $bdd->prepare('DELETE FROM dislikes WHERE id_jeu = ? AND id_membre = ?');
            $del->execute(array($getid,$idmembre));
         } else {
            $ins = $bdd->prepare('INSERT INTO dislikes (id_jeu, id_membre) VALUES (?, ?)');
            $ins->execute(array($getid, $idmembre));
         }
      }
      header('Location: '.$_SERVER['HTTP_REFERER']);
   } else {
      exit('Erreur fatale. ');
   }
} else {
   exit('Erreur fatale. ');
}
    
    
    