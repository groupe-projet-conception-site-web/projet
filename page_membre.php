<?php
  // Initialiser la session
  session_start();
  // Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
  if(!isset($_SESSION["emailconnection"])){
    header("Location: home_page.php");
    exit(); 

  }

// recuperer prenom
$conn3 = mysqli_connect('localhost','root','root','projet') or die (mysqli_error());
$email1=$_SESSION["emailconnection"];
$query2 = "SELECT prenom FROM membres WHERE email='$email1'";
$result2 = mysqli_query($conn3,$query2) or die(mysql_error());
while ($row = $result2->fetch_assoc()) {
  $prenommembre=$row['prenom']."<br>";
}  
?>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <title>page membre</title>
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body id='fondmembre'>
  <!-----Barre de Navigation---->
	<section id="Navigation" >
		<nav class="navbar navbar-expand-lg navbar-light ">
  <a class="navbar-brand" href="#"><img src="images/logo3.jpeg"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="deconnection.php"> SE DECONNECTER </a>
      </li>
      
    </ul>
  </div>
</nav>
  </section>
  <section id='infojeu'>
  <h1> Bienvenue <?php echo $prenommembre; ?> sur <a href="home_page.php">Wearegamer.com</a> </h1>
  <p> Dans ton espace membre, tu pourras voir tes jeux préférés et leur mettre un j'aime ou un je n'aime pas. 
    Tu auras également des infos sur ces derniers afin de t'aider à choisir le prochain jeu que tu joueras !! Amuse toi !!
  </p>
</section>
  <?php
try
{
	// On se connecte à MySQL
	$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'root', 'root');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

// recuperer id membre
$emailmembre=$_SESSION["emailconnection"];
$conn2 = mysqli_connect('localhost','root','root','projet') or die (mysqli_error());
$query1 = "SELECT id FROM membres WHERE email='$emailmembre'";
$result1 = mysqli_query($conn2,$query1) or die(mysql_error());
while ($row = $result1->fetch_assoc()) {
  $idmembre=$row['id']."<br>";
}

// On récupère tout le contenu de la table jeux_video
$reponse = $bdd->query('SELECT * FROM jeu');

// On affiche chaque entrée une à une
while ($donnees = $reponse->fetch())  
{
  // traitement like dislike
  $get_id = $donnees['id'];
   $jeu = $bdd->prepare('SELECT * FROM jeu WHERE id = ?');
   $jeu->execute(array($get_id));
   if($jeu->rowCount() == 1) {
      $jeu = $jeu->fetch();
      $id = $jeu['id'];
      $likes = $bdd->prepare('SELECT id FROM likes WHERE id_jeu = ?');
      $likes->execute(array($id));
      $likes = $likes->rowCount();
      $dislikes = $bdd->prepare('SELECT id FROM dislikes WHERE id_jeu = ?');
      $dislikes->execute(array($id));
      $dislikes = $dislikes->rowCount(); }


?>

 <div> <img id="imagejeu" src="images/<?php echo $donnees['image']; ?>" alt="Photo du jeu" /> </div>
<div id="infojeu">
<h2 id="titrejeu"> <?php echo $donnees['nom']; ?> </h2>
    <p> <strong>Gescription :</strong> <?php echo $donnees['description']; ?> </p> 
    <p> <strong>Genre :</strong> <?php echo $donnees['genre']; ?>    <strong>Console :</strong> <?php echo $donnees['console']; ?></p> 
    <a href="likes.php?t=1&id=<?= $donnees['id'] ?>&idmembre=<?= $idmembre ?>">J'aime</a> (<?= $likes ?>)
   <a href="likes.php?t=2&id=<?= $donnees['id'] ?>&idmembre=<?= $idmembre ?>">Je n'aime pas</a> (<?= $dislikes ?>) 
 
</div>
<?php

}

$reponse->closeCursor(); // Termine le traitement de la requête

?>
<!-- Footer -->
<footer class="page-footer font-small special-color-dark pt-4">
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="#Navigation"> Wearegamer.com</a>
  </div>

</footer>
</body>
</html>