<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <title> Liste Jeux </title>
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
  <!-----Barre de Navigation---->
	<section id="Navigation" >
		<nav class="navbar navbar-expand-lg navbar-light ">
  <a class="navbar-brand" href="#"><img src="images/logo3.jpeg"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="page_admin.php"> RETOUR </a>
      </li>
    </ul>
  </div>
</nav>
	</section>
  <?php
try
{
	// On se connecte à MySQL
	$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'root', 'root');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

// On récupère tout le contenu de la table jeux_video
$reponse = $bdd->query('SELECT * FROM jeu');


// On affiche chaque entrée une à une
while ($donnees = $reponse->fetch())  
{
  // traitement like dislike
  $get_id = $donnees['id'];
   $jeu = $bdd->prepare('SELECT * FROM jeu WHERE id = ?');
   $jeu->execute(array($get_id));
   if($jeu->rowCount() == 1) {
      $jeu = $jeu->fetch();
      $id = $jeu['id'];
      $likes = $bdd->prepare('SELECT id FROM likes WHERE id_jeu = ?');
      $likes->execute(array($id));
      $likes = $likes->rowCount();
      $dislikes = $bdd->prepare('SELECT id FROM dislikes WHERE id_jeu = ?');
      $dislikes->execute(array($id));
      $dislikes = $dislikes->rowCount(); }


?>
 <div> <img id="imagejeu" src="images/<?php echo $donnees['image']; ?>" alt="Photo du jeu" /> </div>
<div id="infojeu">
<h2 id="titrejeu"> <?php echo $donnees['nom']; ?> </h2>
    <p> <strong>Gescription :</strong> <?php echo $donnees['description']; ?> </p> 
    <p> <strong>Genre :</strong> <?php echo $donnees['genre']; ?>    <strong>Console :</strong> <?php echo $donnees['console']; ?></p>
    
     <a href="supprimerjeu.php?id=<?= $donnees['id'] ?>"> Supprimer </a> 
</div>
   


<?php

}

$reponse->closeCursor(); // Termine le traitement de la requête

?>

  
  
  
</body>
</html>